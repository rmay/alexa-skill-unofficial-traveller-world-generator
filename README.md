# Alexa Skill Traveller World Generator
Code for my Alexa Skill Traveller World Generator

**Usage**

The Alexa Skill calls lambda.py

**Testing locally**

Install emulambda. `pip install -e emulambda`
See https://github.com/fugue/emulambda for more information

Run emulambda

`emulambda lambda.lambda_handler test_json/test.json`

Add `-v` for more information.

`emulambda lambda.lambda_handler testjson/test.json -v`


**Data source**

Stuff I found on the Internet. No copyright infringement intended.


**Requirements**

- Python 2.7
- emulambda
- AWS account with credentials on your development machine
- Alexa Developer account

