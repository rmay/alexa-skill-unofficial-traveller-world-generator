"""
Traveller World Generator
"""

from __future__ import print_function
import random
import json
import urllib
import boto3
from boto3.dynamodb.conditions import Key, Attr
from world_gen import WorldUPW

import pdb

VERSION = 1.0

# --------------- Helpers that build all of the responses ----------------------

def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': title,
            'content': output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    """ If we wanted to initialize the session to have some attributes we could
    add those here
    """

    session_attributes = {}
    card_title = "Welcome"
    speech_output = "Welcome to the Unoffical Traveller World Generator. " \
        "Just ask me for a world and I'll give you a randomized UWP."
    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = "For instructions on what you can say, please say help me."
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def handle_session_end_request():
    card_title = "Session Ended"
    speech_output = "Thank you for trying Travller World Generator. " \
                    "Have a nice day! "
    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))

def handle_session_stop_request():
    card_title = "Stop"
    speech_output = "Ok."
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))

# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    #print("on_session_started requestId=" + session_started_request['requestId']
    #      + ", sessionId=" + session['sessionId'])
    print("on_session_started")


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they
    want
    """

    #print("on_launch requestId=" + launch_request['requestId'] +
    #      ", sessionId=" + session['sessionId'])
    # Dispatch to your skill's launch
    return get_welcome_response()


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    print("Version: " + str(VERSION))
    print("intent_name:" + intent_name)

    # Dispatch to your skill's intent handlers
    if intent_name == "WorldGeneratorIntent":
        return generate_uwp(intent, session)
    elif intent_name == "AgainIntent":
        return again_intent(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.FallbackIntent":
        return get_welcome_response()
    elif intent_name == "AMAZON.CancelIntent":
        return handle_session_end_request()
    elif intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent " + intent_name)


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    #print("on_session_ended requestId=" + session_ended_request['requestId'] +
    #      ", sessionId=" + session['sessionId'])
    # add cleanup logic here


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    #print("event.session.application.applicationId=" +
    #      event['session']['application']['applicationId'])

    """
    Uncomment this if statement and populate with your skill's application ID to
    prevent someone else from configuring a skill that sends requests to this
    function.
    """
    # if (event['session']['application']['applicationId'] !=
    #         "amzn1.echo-sdk-ams.app.[unique-value-here]"):
    #     raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])


# --------------- Functions for the core of World UWP Generator  -----------------


def generate_uwp(intent, session):
    card_title = "Traveller World Generator"
    should_end_session = True
    zip_code = ""
    session_attributes = ""

    try:
        myWorld = WorldUPW()
        data = myWorld.generate_upw()
        uwp = data['uwp']
        should_end_session = False
        speech_output = uwp
        reprompt_text = "You can ask for a new world or you can quit by saying 'bye'"

    except Exception as e:
        print("error in generate_uwp: ", e)
        error_message = "I am sorry but somethine went wrong. Please try again or you can quit by saying 'bye'."
        reprompt_text = "Please try again or you can quit by saying 'bye'."
        speech_output = error_message
        should_end_session = False

    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def again_intent(intent, session):
    card_title = "Traveller World Generator"
    should_end_session = False
    speech_output = "Would you like a new world generated?"
    reprompt_text = speech_output
    session_attributes = ""
    
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))

def safe_str(s):
    return 'None' if s is None else str(s)

