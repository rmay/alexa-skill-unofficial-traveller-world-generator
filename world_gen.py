"""
Quick and dirty planet generation for Traveller RPG
"""

import os
from random import randint

class WorldUPW:
    def generate_upw(self):
        try:
            star_port_code = self.__encode_star_port(self.__roll_dice(2, 6))
            has_naval_base = False
            has_scout_base = False
            has_gas_giant = False

            planet_size = self.__roll_dice(2,6) - 2
            planet_code = self.__encode_planet(planet_size)
            atmosphere_code = "0"
            atmosphere_number = 0
            if planet_size > 0:
                atmosphere_number = (self.__roll_dice(2, 6) - 7) + planet_size
                atmosphere_code = self.__encode_atmosphere(atmosphere_number)

            hydrographic_code = "0"
            if planet_size > 0:
                modifier = 7
            if atmosphere_number < 2 or atmosphere_number > 9:
                modifier = 4
            hydrographic_code = self.__encode_hydrographic((self.__roll_dice(2, 6) - modifier) + planet_size)

            population = self.__roll_dice(2, 6)
            population_code = self.__encode_population(population)
            government_code = "0"
            government_number = 0
            if population > 0:
                government_number = self.__roll_dice(2, 6) - 7 + population
                government_code = self.__encode_government(government_number)

            law_level_code = "0"
            if population > 0:
                law_level_code = self.__encode_law_level(self.__roll_dice(2, 6) - 7 + government_number)

            pieces = {'star_port': star_port_code,
                  'planet_size': planet_code,
                  'atmosphere': atmosphere_code,
                  'hydrographic': hydrographic_code,
                  'population': population_code,
                  'government': government_code,
                  'law_level': law_level_code}

            tech_code = self.__encode_tech(pieces)

            pieces['tech'] = tech_code

            uwp = self.__emit_UWP(pieces)
            return {'uwp': uwp, 
                'has_naval_base': has_naval_base, 
                'has_scout_base': has_scout_base, 
                'has_gas_giant': has_gas_giant}
        except Exception as e:
            print(e)
            return e

    def __encode_star_port(self, number):
        port_map = { 2 : "A",
                    3 : "A",
                    4 : "A",
                    5 : "B",
                    6 : "B",
                    7 : "C",
                    8 : "C",
                    9 : "D",
                    10 : "E",
                    11 : "E",
                    12 : "X"
        }
        return port_map[number]

    def __encode_planet(self, number):
        planet_map = { 10: "A",
                     11: "B",
                     12: "C",
                     13: "D",
                     14: "G"
                     }
        if number < 10:
            return str(number)
        else:
            return planet_map[number]

    def __encode_atmosphere(self, number):
        atmo_map = { 10: "A",
                     11: "B",
                     12: "C",
                     13: "D",
                     14: "G"
                     }
        if number < 0:
            return "0"
        elif number < 10:
            return str(number)
        else:
            return atmo_map[number]

    def __encode_hydrographic(self, number):
        if number < 0:
            return "0"
        elif number < 10:
            return str(number)
        else:
            return "A"

    def __encode_population(self, number):
        pop_map = { 10: "A",
                     11: "B",
                     12: "C",
                     13: "C",
                     14: "C"
                   }
        if number < 10:
            return str(number)
        else:
            return pop_map[number]

    def __encode_government(self, number):
        gov_map = { 10: "A",
                    11: "B",
                    12: "C",
                    13: "D",
                    14: "E",
                    15: "F",
                    16: "F"
                  }
        if number < 0:
            return "0"
        elif number < 10:
            return str(number)
        else:
            return gov_map[number]

    def __encode_law_level(self, number):
        if number < 0:
            return "0"
        elif number < 10:
            return str(number)
        else:
            return "9"

    def __encode_tech(self, pieces):
        tech_level = 0
        modifier = 0
        sp = pieces['star_port']
        if sp == "A":
            modifier += 6
        elif sp == "B":
            modifier += 4
        elif sp == "C":
            modifier += 2
        elif sp == "X":
            modifier -= 4

        size = pieces['planet_size']
        if size == "0" or size == "1":
            modifier += 2
        elif size == "2" or size == "3" or size == "4":
            modifier += 1

        atmo = pieces['atmosphere']
        atmo_set = set(['0', '1', '2', '3', 'A', 'B', 'C', 'D', 'E'])
        if atmo in atmo_set:
            modifier += 1

        hyd = pieces['hydrographic']
        if hyd == "9":
            modifier += 1
        elif hyd == "A":
            modifier += 2

        pop = pieces['population']
        pop_set = set(['1', '2', '3', '4', '5'])
        if pop in pop_set:
            modifier += 1
        if pop == "9":
            modifier += 2
        elif pop == "A":
            modifier += 4

        govt = pieces['government']
        if govt == "0" or govt == "5":
            modifier += 1
        elif govt == "D":
            modifier -= 2

        if modifier < 0:
            modifier = 0

        return str(tech_level + modifier)

    def __emit_UWP(self, pieces):
        upw =  "".join([pieces['star_port'],
                        pieces['planet_size'],
                        pieces['atmosphere'],
                        pieces['hydrographic'],
                        pieces['population'],
                        pieces['government'],
                        pieces['law_level'],
                        "-",
                        pieces['tech']
                       ])

        if len(upw) < 9 or len(upw) > 10:
            print(pieces)
            print(len(upw))
            print("         ------------------------- Something is amiss")

        return upw

    def __roll_dice(self, number, sides):
        total = 0
        for x in xrange(number):
            rando = randint(1,sides)
            total += rando
        return total


#myWorld = WorldUPW()
#for x in xrange(100):
#    data = myWorld.generate_upw()
#    print(data['uwp'])
